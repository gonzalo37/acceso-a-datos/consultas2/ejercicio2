<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de selección 2';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección 2</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 1</h3>
                        <p>Número de ciclistas que hay</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta1'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
            <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 2</h3>
                        <p>Número de ciclistas que hay</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta2'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
            <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 3</h3>
                        <p>Edad media de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta3'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 4</h3>
                        <p> La edad media de los del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta4'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
              <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 5</h3>
                        <p> La edad media de los ciclistas por cada equipo</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta5'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                  
                  <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 6</h3>
                        <p> El número de ciclistas por equipo</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta6'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                      
                      <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 7</h3>
                        <p>  El número total de puertos</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta7'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                          
                         <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 8</h3>
                        <p>  El número total de puertos mayores de 1500</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta8'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>   
                             
                           <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 9</h3>
                        <p> Listar el nombre de los equipos que tengan más de 4 ciclistas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta9'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>                
                
                      <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 10</h3>
                        <p> Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta10'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div> 
                   
                    <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 11</h3>
                        <p> Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta11'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div> 
                        
                    <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 12</h3>
                        <p> Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta12'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/segundaconsulta12'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>          
                        
        </div>
    </div>
</div>
