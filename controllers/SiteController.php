<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Equipo;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("COUNT(*) AS numciclistas"),
            'pagination'=>[
                'pageSize'=>1,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista AS numciclistas",
        ]);
        
        
    }
    
    public function actionSegundaconsulta1(){
        
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT COUNT(*)AS numciclistas FROM ciclista ",
            
            'pagination'=>[
                'pageSize' => 1,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numciclistas'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) FROM ciclista AS numciclistas",
        ]);
}
 public function actionConsulta2(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("COUNT(*) AS numciclistas")
                ->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>1,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numciclistas'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista AS numciclistas WHERE nomequipo='Banesto'",
        ]);
        
        
    }
    
    public function actionSegundaconsulta2(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT COUNT(*)AS numciclistas FROM ciclista WHERE nomequipo='Banesto'",
         
            'pagination'=>[
                'pageSize' => 1,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numciclistas'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
}

public function actionConsulta3(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("AVG(edad) AS mededad"),
            'pagination'=>[
                'pageSize'=>1,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mededad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad)AS mededad FROM ciclista;",
        ]);
        
        
    }
    
     public function actionSegundaconsulta3(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"select avg(edad) as mededad from ciclista",
         
            'pagination'=>[
                'pageSize' => 1,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mededad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad)AS mededad FROM ciclista;",
        ]);
}
    
    public function actionConsulta4(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("AVG(edad) AS mededad")
                ->where("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>1,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mededad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>" La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad) FROM ciclista AS mededad WHERE nomequipo='Banesto'",
        ]);
        
        
    }
    
    public function actionSegundaconsulta4(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"select avg(edad) as mededad from ciclista where nomequipo='Banesto'",
         
            'pagination'=>[
                'pageSize' => 1,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mededad'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>"SELECT AVG(edad)AS mededad FROM ciclista WHERE nomequipo='Banesto'",
        ]);
}

public function actionConsulta5(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("AVG(edad) AS mededad")
                ->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>1,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mededad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>" La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]);
        
        
    }
    
    public function actionSegundaconsulta5(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"select avg(edad) as mededad from ciclista group by nomequipo",
         
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mededad'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) FROM ciclista GROUP BY nomequipo'",
        ]);
}

public function actionConsulta6(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Ciclista::find()->select("COUNT(edad) AS numciclistas")
                ->groupBy('nomequipo'),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numciclistas'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>" El número de ciclistas por equipo",
            "sql"=>"SELECT COUNT(*) FROM ciclista GROUP BY nomequipo",
        ]);
        
        
    }
    
     public function actionSegundaconsulta6(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"select avg(edad) as mededad from ciclista group by nomequipo",
         
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mededad'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>"SELECT AVG(edad) FROM ciclista GROUP BY nomequipo'",
        ]);
}

public function actionConsulta7(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Puerto::find()->select("COUNT(*) AS numpuerto"),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numpuerto'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>" El número total de puertos",
            "sql"=>"SELECT COUNT(*) as numpuerto FROM puerto",
        ]);
        
        
    }
    
    public function actionSegundaconsulta7(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"select count(*) as numpuerto from puerto",
         
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numpuerto'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>"SELECT COUNT(*) as numpuerto FROM puerto",
        ]);
}

public function actionConsulta8(){
        $dataprovider=new ActiveDataProvider([
            'query'=>Puerto::find()->select("COUNT(*) AS numpuerto")
                ->where("altura>1500"),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numpuerto'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>" El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura>1500",
        ]);
        
        
    }
    
    public function actionSegundaconsulta8(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"select count(*) as numpuerto from puerto where altura>1500",
         
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numpuerto'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>"SELECT COUNT(*) FROM puerto WHERE altura>1500",
        ]);
}

public function actionConsulta9(){
        $dataprovider=new ActiveDataProvider([
            'query'=> Equipo::find()->select("nomequipo")
                ->groupBy('nomequipo')
                ->having('COUNT(*)>4'),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>" Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>" SELECT nomequipo FROM equipo GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
        
        
    }
    
    public function actionSegundaconsulta9(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>" select nomequipo from equipo group by nomequipo having count(*)>4",
         
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>"SELECT nomequipo FROM equipo GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
}


public function actionConsulta10(){
        $dataprovider=new ActiveDataProvider([
            'query'=> Equipo::find()->select("nomequipo")
                ->groupBy('nomequipo')
                ->having('COUNT(*)>4 AND edad BETWEEN 28 AND 32'),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>" Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>" SELECT nomequipo FROM equipo GROUP BY nomequipo HAVING COUNT(*)>4 AND edad BETWEEN 28 AND 32",
        ]);
        
        
    }
    
    public function actionSegundaconsulta10(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>" select nomequipo from equipo group by nomequipo having count(*)>4 and edad between 28 and 32",
         
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>"SELECT nomequipo FROM equipo GROUP BY nomequipo HAVING COUNT(*)>4 AND edad BETWEEN 28 AND 32",
        ]);
}

public function actionConsulta11(){
        $dataprovider=new ActiveDataProvider([
            'query'=> Etapa::find()->select("numetapa")
                ->groupBy('dorsal'),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>" Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>" SELECT numetapa FROM etapa GROUP BY dorsal",
        ]);
        
        
    }
    
     public function actionSegundaconsulta11(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>" select numetapa from etapa group by dorsal",
         
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>"SELECT numetapa FROM etapa GROUP BY dorsal",
        ]);
}

public function actionConsulta12(){
        $dataprovider=new ActiveDataProvider([
            'query'=> Etapa::find()->select("dorsal")
                ->where('numetapa>1'),
            'pagination'=>[
                'pageSize'=>5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>" Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>" SELECT dorsal FROM etapa WHERE numetapa>1",
        ]);
        
        
    }
    
    public function actionSegundaconsulta12(){
       $numero = Yii::$app->db
                ->createCommand("select dorsal from etapa where numetapa>1")//contar registros
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT dorsal FROM etapa WHERE numetapa>1",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
         return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa WHERE numetapa>1",
        ]);
}





}